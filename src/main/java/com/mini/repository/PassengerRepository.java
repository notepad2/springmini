package com.mini.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mini.model.Passenger;

public interface PassengerRepository extends JpaRepository<Passenger, Long> {

	
}
