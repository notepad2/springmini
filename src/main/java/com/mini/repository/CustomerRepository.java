package com.mini.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mini.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, String> {

	Customer findByFirstName(String firstName);
	
}
