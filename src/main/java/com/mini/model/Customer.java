package com.mini.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="customers")
public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	
	@Column(name="first-Name")
	private String firstName;
	
	@Column(name="password")
	private String password;
	
	@Column(name="email_id")
	private String email;
	
	public Customer() {
		super();
	}
	public Customer(String firstName, String password) {
		super();
		this.firstName = firstName;
		this.password = password;
	}
	public Customer( String firstName, String password, String email) {
		super();
		this.firstName = firstName;
		this.password = password;
		this.email = email;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "Customer [id=" + id + ", firstName=" + firstName + ", password=" + password + ", email=" + email + "]";
	}
}
