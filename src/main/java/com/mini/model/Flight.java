package com.mini.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name ="Flight")
public class Flight {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name="source")
	private String source;
	
	@Column(name = "detination")
	private String detination;
	
	@Column(name="NumberOp")
	private int numberOp;
	
	@Column(name = "date")
	private String date;

	public Flight(long id, String source, String detination, int numberOp, String date) {
		super();
		this.id = id;
		this.source = source;
		this.detination = detination;
		this.numberOp = numberOp;
		this.date = date;
	}

	public Flight() {
		super();
	}

	public Flight(String source, String detination, int numberOp, String date) {
		super();
		this.source = source;
		this.detination = detination;
		this.numberOp = numberOp;
		this.date = date;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDetination() {
		return detination;
	}

	public void setDetination(String detination) {
		this.detination = detination;
	}

	public int getNumberOp() {
		return numberOp;
	}

	public void setNumberOp(int numberOp) {
		this.numberOp = numberOp;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Flight [id=" + id + ", source=" + source + ", detination=" + detination + ", numberOp=" + numberOp
				+ ", date=" + date + "]";
	}
}
