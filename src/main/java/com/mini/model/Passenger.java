package com.mini.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="passengers")
public class Passenger {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name="frm")
	private String frm;
	
	@Column(name="tom")
	private String tom;

	@Column(name="nop")
	private int nop;
	
	@Column(name="name")	
	private String name;

	@Column(name="Email")
	private String email;

	public Passenger(long id, String frm, String tom, int nop, String name, String email) {
		super();
		this.id = id;
		this.frm = frm;
		this.tom = tom;
		this.nop = nop;
		this.name = name;
		this.email = email;
	}
	
	
	public Passenger(String frm, String tom, int nop, String name, String email) {
		super();
		this.frm = frm;
		this.tom = tom;
		this.nop = nop;
		this.name = name;
		this.email = email;
	}


	public Passenger() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFrm() {
		return frm;
	}

	public void setFrm(String frm) {
		this.frm = frm;
	}

	public String getTom() {
		return tom;
	}

	public void setTom(String tom) {
		this.tom = tom;
	}

	public int getNop() {
		return nop;
	}

	public void setNop(int nop) {
		this.nop = nop;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Passenger [id=" + id + ", frm=" + frm + ", tom=" + tom + ", nop=" + nop + ", name=" + name + ", email="
				+ email + "]";
	}
}
