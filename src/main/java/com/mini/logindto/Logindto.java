package com.mini.logindto;

import jakarta.persistence.Column;

public class Logindto {
	private String firstName;
	private String password;
	
	
	public Logindto(String firstName, String password) {
		super();
		this.firstName = firstName;
		this.password = password;
	}

	public Logindto() {
		super();
	}

	public Logindto(String firstName, String password, String email) {
		super();
		this.firstName = firstName;
		this.password = password;
		
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Logindto [firstName=" + firstName + ", password=" + password + "]";
	}
	
}
