package com.mini.dto;

public class CarDto {
	private int id ;
	private String nameOfCS;
	private String fromDate ;
	private String toDate ;
	private String nameOfCar ;
	public CarDto(int id, String nameOfCS, String fromDate, String toDate, String nameOfCar) {
		super();
		this.id = id;
		this.nameOfCS = nameOfCS;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.nameOfCar = nameOfCar;
	}
	public CarDto() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNameOfCS() {
		return nameOfCS;
	}
	public void setNameOfCS(String nameOfCS) {
		this.nameOfCS = nameOfCS;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getNameOfCar() {
		return nameOfCar;
	}
	public void setNameOfCar(String nameOfCar) {
		this.nameOfCar = nameOfCar;
	}
	@Override
	public String toString() {
		return "CarDto [id=" + id + ", nameOfCS=" + nameOfCS + ", fromDate=" + fromDate + ", toDate=" + toDate
				+ ", nameOfCar=" + nameOfCar + "]";
	}
	
}
