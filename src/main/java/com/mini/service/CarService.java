package com.mini.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.mini.dto.CarDto;

@Service
public interface CarService {

	CarDto carDetails(CarDto carDto);


	List<CarDto> getAllCars();


	CarDto getCarBookerById(Integer getById);



	Map<String, Boolean> deleteById(Integer delById);


	CarDto updateDetailsById(Integer id, CarDto carDtoUpdate);

}
