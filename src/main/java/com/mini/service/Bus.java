package com.mini.service;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.mini.dto.BusDto;
import com.mini.model.Passenger;

@Service
public interface Bus {
	 public BusDto passengerDetails( BusDto busDto );
}
