package com.mini.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mini.dto.CarDto;
import com.mini.exception.ResourceNotFoundException;
import com.mini.mapper.Mapper;
import com.mini.model.Car;
import com.mini.repository.CarRepository;

@Service
public class CarServiceImpl implements CarService {

	@Autowired
	private CarRepository carRepository;
	
	@Override
	public CarDto carDetails(CarDto carDto) {
		Car car = Mapper.mapToCar(carDto);
		Car car2 = carRepository.save(car);
		CarDto carDto2	= Mapper.mapToCarDto(car2);	
		return carDto2;
	}

	@Override
	public List<CarDto> getAllCars() {
		List<Car> cars = carRepository.findAll();
		return cars.stream().map((carDto) -> Mapper.mapToCarDto(carDto)).collect(Collectors.toList());
	}

	@Override
	public CarDto getCarBookerById(Integer getById) {
		Car car = carRepository.findById(getById)
				.orElseThrow(()-> new ResourceNotFoundException("CarBooking With this id Not found : "+getById));
		return Mapper.mapToCarDto(car);	
	}

	@Override
	public Map<String, Boolean> deleteById(Integer delById) {
		Car car = carRepository.findById(delById)
									.orElseThrow(()-> new ResourceNotFoundException( delById+"Id Not Found"));
				carRepository.delete(car);
		Map<String, Boolean> response = new HashMap<>();
		 response.put("Deleted", Boolean.TRUE);
		 return response;
					
	}

	@Override
	public CarDto updateDetailsById(Integer id, CarDto carDtoUpdate) {
		Car car = carRepository.findById(id)
				.orElseThrow(()->new ResourceNotFoundException(id+"Id Not Found") );
		car.setNameOfCS(carDtoUpdate.getNameOfCS());
		car.setFromDate(carDtoUpdate.getFromDate());
		car.setToDate(carDtoUpdate.getToDate());
		car.setNameOfCar(carDtoUpdate.getNameOfCar());
		
			Car car2 =carRepository.save(car);
			return Mapper.mapToCarDto(car2);
		
	}
}
