package com.mini.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.mini.dto.BusDto;
import com.mini.mapper.Mapper;
import com.mini.model.Passenger;
import com.mini.repository.PassengerRepository;

@Service
public class BusImpl implements Bus {

	@Autowired
	private PassengerRepository passengerRepo;
	
	@Autowired
	JavaMailSender mailSender ;
	
	
	@Override
	public BusDto passengerDetails(BusDto busDto) {
	
		Passenger passenger = Mapper.mapTopassenger(busDto);
		String userEmail = busDto.getEmail();
		 Passenger  savedPassenger  =passengerRepo.save(passenger);
		
		BusDto busDto2 = Mapper.mapToBusDto(savedPassenger);
		 sendEmail("kaarthira03@gmail.com", userEmail, "Booking Details","Your Bus  booking Confirmed" );
		return busDto2;
	}
	
public void sendEmail(String fromEmail,String toEmail,String subject,String body) {
		
		SimpleMailMessage mailMessage=new SimpleMailMessage();
		mailMessage.setFrom(fromEmail);
		mailMessage.setTo(toEmail);
		mailMessage.setSubject(subject);
		mailMessage.setText(body);
		
		mailSender.send(mailMessage);
	}
}
