package com.mini.controller;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mini.dto.BusDto;
import com.mini.dto.CarDto;
import com.mini.exception.ResourceNotFoundException;
import com.mini.logindto.Logindto;
import com.mini.mapper.Mapper;
import com.mini.model.Car;
import com.mini.model.Customer;
import com.mini.model.Flight;
import com.mini.model.Passenger;
import com.mini.repository.CarRepository;
import com.mini.repository.CustomerRepository;
import com.mini.repository.FlightRepository;
import com.mini.repository.PassengerRepository;
import com.mini.service.Bus;
import com.mini.service.CarService;


@RestController
@CrossOrigin
@RequestMapping("/api/v1/")
public class CustomerController {
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private CarService carService;
	
	@Autowired
	private FlightRepository flightRepository;
	
	@Autowired
	private Bus bus;  // BusService
	 
	//Login Page 
	@PostMapping("/login")
	public ResponseEntity<?> loginUsers( @RequestBody Customer customerdata){
		Customer cs=customerRepository.findByFirstName(customerdata.getFirstName());
		if (cs.getPassword().equals(customerdata.getPassword())) {
			return ResponseEntity.ok(cs);
		}
			return (ResponseEntity<?>) ResponseEntity.internalServerError();
	}	
	//For Flight
	@PostMapping("/flight")
	public Flight createFlight(@RequestBody Flight flight ) {
		return flightRepository.save(flight);
	}
	
	//SignUp Save Details
	@PostMapping("/customers")
	public Customer createCustomer(@RequestBody Customer customer) {
		return customerRepository.save(customer);
	}
	
	//For Cars
	@PostMapping("/car")
	public CarDto carDetails(@RequestBody CarDto carDto ) {
			CarDto carDto2 = carService.carDetails(carDto);
			return carDto2;
	}
		
	// For Passengers Bus
	@PostMapping("/passengers")
	public BusDto passengerDetails(@RequestBody BusDto busDto ) {
		
	BusDto savedPassengerDto = bus.passengerDetails( busDto);
		return savedPassengerDto;
	}
	
	// Getting Customers SignUp
	@GetMapping("/customers")
	public List<Customer> getAllCustomers(){
		return customerRepository.findAll();
	}
	
	// Getting FlightBookings
	@GetMapping("/flight")
	public List<Flight> getBookings(){
		return flightRepository.findAll();
	}
	
	//Get FlightBooking By Id
	@GetMapping("/flight/{id}")
	public ResponseEntity<Flight> getFlightDetailsById(@PathVariable Long id){
		Flight flight = flightRepository.findById(id).
				orElseThrow(()-> new ResourceNotFoundException("Flight Booking With id not found : "+id));
		return ResponseEntity.ok(flight);
	}
	
	//Update FlightBooking By Id
	
	@PutMapping("flight/{id}")
	public ResponseEntity<Flight> getFlightBookings(@PathVariable Long id , @RequestBody Flight flightBdetail){
		Flight flight = flightRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Flight Booking with id not found : " +id));
			
			flight.setSource(flightBdetail.getSource());
			flight.setDetination(flightBdetail.getDetination());
			flight.setNumberOp(flightBdetail.getNumberOp());
			flight.setDate(flightBdetail.getDate());
			
			Flight saveFlightDetails =flightRepository.save(flight);
			return ResponseEntity.ok(saveFlightDetails);
	}
	//Delete By ID
	@DeleteMapping("flight/{id}")
	public ResponseEntity< Map<String, Boolean>> deleteFlightByID(@PathVariable Long id){
		Flight flight = flightRepository.findById(id).
				orElseThrow( () -> new ResourceNotFoundException("Booking With Flight Id Not Exist "+id));
		flightRepository.delete(flight);
		
		Map<String, Boolean> sendResponse = new LinkedHashMap<>();
		sendResponse.put("Id Deleted ", Boolean.TRUE);
		return	ResponseEntity.ok(sendResponse);
	}
	
	
	//Get all Car Booking Details
	@GetMapping("/car")
	public ResponseEntity<List<CarDto>>  getCarRentals(){
		List<CarDto> carDtos = carService.getAllCars();
		return ResponseEntity.ok(carDtos);
	}
	
	//Get Car Rentals by id 
	@GetMapping("car/{id}")
	public ResponseEntity<CarDto> getCarRentalsByid(@PathVariable("id") Integer getById){
		CarDto carDto = carService.getCarBookerById(getById);
		return ResponseEntity.ok(carDto);
	}
	
	//Delete CarBookingRentals By ID
	@DeleteMapping("car/{id}")
	public  ResponseEntity<Map<String, Boolean>> deleteCarBooking(@PathVariable("id") Integer delById){
				Map<String, Boolean> map	=carService.deleteById(delById);
							return ResponseEntity.ok(map);
	}
	   
	//Updating CarBooking Rentals By ID
	@PutMapping("/car/{id}")
	public ResponseEntity<CarDto> updateCarRentals(@PathVariable Integer id , @RequestBody CarDto carDtoUpdate){
							CarDto carDto	=carService.updateDetailsById(id, carDtoUpdate);
							return ResponseEntity.ok(carDto);
	}
}
